Restauration GS2E 
---------------

Ce projet est basé sur la solution <a href="https://www.odoo.com">Odoo</a>. L'ERP Open Source n°1 dans le monde.

Odoo est une suite d'applications web de gestion. Les principales applications couvrent la plupart des besoins
en terme de gestion quotidienne d'une entreprise. Celles-ci concernent le CRM (Gestion de relation clientelle), un 
CMS (Content Management System) pour la création d'un site web en deux click, la vente, les achats, la comptabilité et
bien d'autres.

Odoo est également un puissant Framework. En effet sa structure modulaire lui permet de s'adapter à toute autre domaine 
qui n'est pas couvert par ses modules de base.

Installation
----------------
Pour une installation standard, veuillez suivre <a href="https://www.odoo.com/documentation/13.0/setup/install.html">les instructions de configuration </a> de la documentation.

Pour apprendre le logiciel, nous vous recommandons le Odoo eLearning, ou Scale-up, le jeu d'entreprise. Les développeurs peuvent commencer par les didacticiels pour les développeurs 